/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Hashtable;
import java.util.Scanner;

/**
 *
 * @author Margarita
 */
public class ByteDec {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       int op;
       
       Scanner entrada = new Scanner(System.in);
      do{
       System.out.println(" ");
       System.out.println("1.- Byte a decimal");
       System.out.println("2.- Decimal a byte ");
       System.out.println("3.- Salir");
       op=entrada.nextInt();
       
       switch(op) 
       {
       case 1:
       String byte1;
   
        System.out.println("Ingresa el byte a convetir a decimal:");
        byte1=entrada.next();
        
        if(byte1.length() == 8){
            
        
        long num = Long.parseLong(byte1); 
        long decimal = 0; 
        int contador = 1; 
        long auxdecimal; 
        
        while (num > 0){
            auxdecimal = num % 2; 
            decimal = decimal + auxdecimal * contador;
            num = num / 10; 
            contador = contador * 2;
        }
       
        System.out.println("El numero byte "+byte1+" en decimal es: " +decimal);   
        }
        else
        {
            System.err.println("El byte introducido " +byte1+ " no tiene el tamaño de 8 bits, verifica ");   
        }
        break;
        
        case 2:
            long numero;
            Scanner leer = new Scanner(System.in);
            System.out.println("Ingresa el numero decimal a convetir a byte: ");
            numero=leer.nextLong();
         
         
            long aux = numero; 
            String byte2="";
            
            while (aux > 0){
            byte2 = aux % 2 + byte2; 
            aux = aux /2; 
            }
            
            System.out.println("El resultado de la conversion de decimal a byte es:");
            int longitud = byte2.length();
            int caracteres = 8 - longitud;
             for (int i=0; i<caracteres; i++)
                {
                System.out.print('0');
                }
            System.out.print(byte2);
            }
      }while(op != 3);
    }
    }
    

